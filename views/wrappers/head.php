<!DOCTYPE html>
<html>
	<head>
		<title>
			<?php echo (isset($state['page_title'])) ? $state['page_title'] : 'PokeShop'; ?>
		</title>

		<link rel="icon" href="assets/img/favicon.ico">

		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="../assets/css/united.min.css">
		<link rel="stylesheet" href="../assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="../assets/css/global.css">

		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/notify.min.js"></script>

	</head>
	<body>
