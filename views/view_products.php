<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<legend>Browse Pokemons</legend>
			<div class="row pokemons">
				<!-- Pokemons will be appended here -->
			</div>
		</div>
		<div class="col-md-offset-9 col-sm-offset-8 col-md-3 col-sm-4" style="position: fixed">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<i class="fa fa-shopping-cart"></i> My Shopping Cart
				</div>
				<div class="panel-body" style="height: 70vh; overflow: auto">
					<table class="table shopping-cart">
						<thead>
							<tr>
								<th>Pokemon Name</th>
								<th>Price</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Total: </th>
								<th class="total-bill"></th>
							</tr>
						</tfoot>
						<tbody>
							<!-- <tr>
								<td colspan="2" class="text-center"><small>Cart is empty :(</small></td>
							</tr> -->
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<button data-toggle="modal" data-target=".checkout-modal" class="btn btn-sm btn-primary check-out-btn" disabled><i class="fa fa-money fa-fw"></i> Checkout</button>
					<button onclick="shoppingCartMethods.clearCart()" class="btn btn-sm btn-warning clear-btn"><i class="fa fa-trash fa-fw"></i> Clear</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade checkout-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<i class="fa fa-fw fa-money"></i> Checkout
			</div>
			<div class="modal-body">
				<table class="table">
					<thead>
						<tr>
							<th>Pokemon Name</th>
							<th>Price</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Total: </th>
							<th class="modal-total"></th>
						</tr>
					</tfoot>
					<tbody>
						
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary final-pay"><i class="fa fa-paypal"></i> Pay automatically using Paypal</button>
			</div>
		</div>
	</div>
</div>
<script>

	var shoppingCart = [];
	var pokemons = [];
	var currentOffset = 0;
	var endOfResults = false;
	var config = {
		limit : 21
	}
	var totalBill = 0;

	var shoppingCartMethods = {
		addItem : function(pokemonObj) {
			shoppingCart.push(pokemonObj);
			$('.shopping-cart tbody').append(
				"<tr>" +
					"<td>" + pokemonObj.name + "</td>" +
					"<td>" + pokemonObj.price + "</td>" +
				"</tr>"
			);
			$('.shopping-cart .total-bill').html(totalBill += parseFloat(pokemonObj.price));
		},
		clearCart : function() {
			shoppingCart = [];
			$('.shopping-cart tbody').empty();
			totalBill = 0;
			$('.shopping-cart .total-bill').html(totalBill);
			$('.pokemon .add-to-cart').each(function() {
				$(this)
					.removeClass('btn-success')
					.addClass('btn-primary')
					.attr('disabled', false)
					.html('<i class="fa fa-shopping-cart"></i> Add to Cart');
			});
		}
	}

	var pokemonMethods = {
		getPokemonById : function(pokemonId) {
			var pokemonMatched;

			$(pokemons).each(function(key, pokemon) {

				if (pokemon.id == pokemonId) {
					pokemonMatched =  pokemon;
				}

				
			});

			return pokemonMatched;
		}
	}

	function parsePokemons(pokemons) {

		$(pokemons).each(function(key, val) {
			$('.pokemons').append(
				'<div class="col-md-4 col-sm-12 pokemon">' +
					'<div class="thumbnail">' +
						'<img class="img-responsive" src="../assets/img/pokemons/'+val.img_file_name+'" alt="">' +
						'<div class="caption">' +
							'<h4 class="text-center">'+val.name+'</h4>' +
							'<h5 class="text-center"><i class="fa fa-dollar"></i> '+val.price+'</h5>' +
							'<table class="table">' +
								'<tbody>' +
									'<tr>' +
										'<th>Attack</th>' +
										'<td>'+val.attack+'</td>' +
									'</tr>' +
									'<tr>' +
										'<th>Defense</th>' +
										'<td>'+val.defense+'</td>' +
									'</tr>' +
									'<tr>' +
										'<th>Type</th>' +
										'<td>'+val.type+'</td>' +
									'</tr>' +
								'</tbody>' +
							'</table>' +
							'<div class="row">' +
								'<div class="col-md-offset-6 col-md-6">' +
									'<button data-pokemon-id="'+val.id+'" class="btn btn-sm btn-primary add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>'
			);
		});

	}

	function getPokemons() {

		$.ajax({
			url : '../rest/get_pokemons',
			data : {
				offset : currentOffset,
				limit : config.limit
			}
		}).done(function(response) {
			
			if (response.length < config.limit) {
				console.log('End of results');
				endOfResults = true;
			}

			parsePokemons(response);

			$(response).each(function(key, pokemon) {
				pokemons.push(pokemon);
			});

			currentOffset += config.limit;

		}).fail(function() {
			console.log('request failed');
		});

	}

	$(document).ready(function() {


		getPokemons();

		/* Add Item to Cart */
		$('body').on('click', '.pokemon .add-to-cart', function() {

			$(this).attr('disabled', true);
			$(this).html('<i class="fa fa-pulse fa-spinner fa-fw"></i> Adding to cart');
			$('.check-out-btn').attr('disabled', false);

			var pokemonId = $(this).attr('data-pokemon-id');
			var pokemonSelected = pokemonMethods.getPokemonById(pokemonId);

			shoppingCartMethods.addItem(pokemonSelected);

			$.notify('Added item to cart.', 'success');
			$(this).removeClass('btn-primary').addClass('btn-success');
			$(this).html('<i class="fa fa-check fa-fw"></i> Added to cart');
		});

		/* Checkout */
		$('.check-out-btn').click(function() {
			$('.checkout-modal .modal-content tbody').empty();
			$(shoppingCart).each(function(key, pokemonObj) {
				$('.checkout-modal .modal-content tfoot .modal-total').html(totalBill);
				$('.checkout-modal .modal-content tbody').append(
						"<tr>" +
							"<td>" + pokemonObj.name + "</td>" +
							"<td>" + pokemonObj.price + "</td>" +
						"</tr>"
				);
			});
		});

		/* Final Payment */
		$('.final-pay').click(function() {

			var $thisButton = $(this);

			$thisButton.html("<i class='fa fa-pulse fa-spinner'></i> Please wait...").addClass('disabled');

			setTimeout(function() {
				$thisButton
					.removeClass('btn-primary')
					.addClass('btn-success')
					.html("<i class='fa fa-check'></i> Transaction Completed.");
			}, 1000);

			setTimeout(function() {
				window.location.reload();
			}, 2000);
		});

		/* Infinite Scrolling */
		$(window).scroll(function() {

			/* if the user scrolls at the bottom of the page */
			if ($(window).scrollTop() == $(document).height() - $(window).height()) {
			    if (!endOfResults) {
			    	getPokemons();
			    }
			}
		});
		
	});
</script>