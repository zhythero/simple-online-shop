-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2015 at 08:08 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dummy`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemons`
--

CREATE TABLE IF NOT EXISTS `pokemons` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `attack` smallint(6) NOT NULL,
  `defense` smallint(6) NOT NULL,
  `type` varchar(100) NOT NULL,
  `price` double NOT NULL DEFAULT '20',
  `img_file_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemons`
--

INSERT INTO `pokemons` (`id`, `name`, `attack`, `defense`, `type`, `price`, `img_file_name`) VALUES
(1, 'Bulbasaur', 49, 49, 'grass', 20, '001 Bulbasaur.png'),
(2, 'Ivysaur', 62, 63, 'grass', 20, '002 Ivysaur.png'),
(3, 'Venusaur', 82, 83, 'grass', 20, '003 Venusaur.png'),
(4, 'Charmander', 52, 43, 'fire', 20, '004 Charmander.png'),
(5, 'Charmeleon', 64, 58, 'fire', 20, '005 Charmeleon.png'),
(6, 'Charizard', 84, 78, 'fire', 20, '006 Charizard.png'),
(7, 'Squirtle', 48, 65, 'water', 20, '007 Squirtle.png'),
(8, 'Wartortle', 63, 80, 'water', 20, '008 Wartortle.png'),
(9, 'Blastoise', 83, 100, 'water', 20, '009 Blastoise.png'),
(10, 'Caterpie', 30, 35, 'bug', 20, '010 Caterpie.png'),
(12, 'Butterfree', 45, 50, 'bug', 20, '012 Butterfree.png'),
(13, 'Weedle', 35, 30, 'bug', 20, '013 Weedle.png'),
(16, 'Pidgey', 45, 40, 'normal', 20, '016 Pidgey.png'),
(17, 'Pidgeotto', 60, 55, 'normal', 20, '017 Pidgeotto.png'),
(18, 'Pidgeot', 80, 75, 'normal', 20, '018 Pidgeot.png'),
(19, 'Rattata', 56, 35, 'normal', 20, '019 Rattata.png'),
(20, 'Raticate', 81, 60, 'normal', 20, '020 Raticate.png'),
(21, 'Spearow', 60, 30, 'normal', 20, '021 Spearow.png'),
(22, 'Fearow', 90, 65, 'normal', 20, '022 Fearow.png'),
(23, 'Ekans', 60, 44, 'poison', 20, '023 Ekans.png'),
(24, 'Arbok', 85, 69, 'poison', 20, '024 Arbok.png'),
(26, 'Raichu', 90, 55, 'electric', 20, '026 Raichu.png'),
(27, 'Sandshrew', 75, 85, 'ground', 20, '027 Sandshrew.png'),
(28, 'Sandslash', 100, 110, 'ground', 20, '028 Sandslash.png'),
(29, 'Nidoran', 47, 52, 'poison', 20, '029 Nidoran.png'),
(31, 'Nidoqueen', 82, 87, 'poison', 20, '031 Nidoqueen.png'),
(32, 'Nidoran', 57, 40, 'poison', 20, '032 Nidorano.png'),
(34, 'Nidoking', 92, 77, 'poison', 20, '034 Nidoking.png'),
(38, 'Ninetales', 76, 75, 'fire', 20, '038 Ninetales.png'),
(41, 'Zubat', 45, 35, 'poison', 20, '041 Zubat.png'),
(42, 'Golbat', 80, 70, 'poison', 20, '042 Golbat.png'),
(46, 'Paras', 70, 55, 'bug', 20, '046 Paras.png'),
(47, 'Parasect', 95, 80, 'bug', 20, '047 Parasect.png'),
(48, 'Venonat', 55, 50, 'bug', 20, '048 Venonat.png'),
(49, 'Venomoth', 65, 60, 'bug', 20, '049 Venomoth.png'),
(50, 'Diglett', 55, 25, 'ground', 20, '050 Diglett.png'),
(51, 'Dugtrio', 80, 50, 'ground', 20, '051 Dugtrio.png'),
(52, 'Meowth', 45, 35, 'normal', 20, '052 Meowth.png'),
(53, 'Persian', 70, 60, 'normal', 20, '053 Persian.png'),
(54, 'Psyduck', 52, 48, 'water', 20, '054 Psyduck.png'),
(55, 'Golduck', 82, 78, 'water', 20, '055 Golduck.png'),
(56, 'Mankey', 80, 35, 'fighting', 20, '056 Mankey.png'),
(57, 'Primeape', 105, 60, 'fighting', 20, '057 Primeape.png'),
(59, 'Arcanine', 110, 80, 'fire', 20, '059 Arcanine.png'),
(60, 'Poliwag', 50, 40, 'water', 20, '060 Poliwag.png'),
(62, 'Poliwrath', 85, 95, 'water', 20, '062 Poliwrath.png'),
(65, 'Alakazam', 50, 45, 'psychic', 20, '065 Alakazam.png'),
(66, 'Machop', 80, 50, 'fighting', 20, '066 Machop.png'),
(68, 'Machamp', 130, 80, 'fighting', 20, '068 Machamp.png'),
(69, 'Bellsprout', 75, 35, 'grass', 20, '069 Bellsprout.png'),
(71, 'Victreebel', 105, 65, 'grass', 20, '071 Victreebel.png'),
(72, 'Tentacool', 40, 35, 'water', 20, '072 Tentacool.png'),
(73, 'Tentacruel', 70, 65, 'water', 20, '073 Tentacruel.png'),
(74, 'Geodude', 80, 100, 'rock', 20, '074 Geodude.png'),
(76, 'Golem', 110, 130, 'rock', 20, '076 Golem.png'),
(77, 'Ponyta', 85, 55, 'fire', 20, '077 Ponyta.png'),
(78, 'Rapidash', 100, 70, 'fire', 20, '078 Rapidash.png'),
(79, 'Slowpoke', 65, 65, 'water', 20, '079 Slowpoke.png'),
(80, 'Slowbro', 75, 110, 'water', 20, '080 Slowbro.png'),
(81, 'Magnemite', 35, 70, 'electric', 20, '081 Magnemite.png'),
(82, 'Magneton', 60, 95, 'electric', 20, '082 Magneton.png'),
(84, 'Doduo', 85, 45, 'normal', 20, '084 Doduo.png'),
(85, 'Dodrio', 110, 70, 'normal', 20, '085 Dodrio.png'),
(86, 'Seel', 45, 55, 'water', 20, '086 Seel.png'),
(87, 'Dewgong', 70, 80, 'water', 20, '087 Dewgong.png'),
(88, 'Grimer', 80, 50, 'poison', 20, '088 Grimer.png'),
(89, 'Muk', 105, 75, 'poison', 20, '089 Muk.png'),
(91, 'Cloyster', 95, 180, 'water', 20, '091 Cloyster.png'),
(92, 'Gastly', 35, 30, 'ghost', 20, '092 Gastly.png'),
(94, 'Gengar', 65, 60, 'ghost', 20, '094 Gengar.png'),
(95, 'Onix', 45, 160, 'rock', 20, '095 Onix.png'),
(96, 'Drowzee', 48, 45, 'psychic', 20, '096 Drowzee.png'),
(97, 'Hypno', 73, 70, 'psychic', 20, '097 Hypno.png'),
(98, 'Krabby', 105, 90, 'water', 20, '098 Krabby.png'),
(99, 'Kingler', 130, 115, 'water', 20, '099 Kingler.png'),
(100, 'Voltorb', 30, 50, 'electric', 20, '100 Voltorb.png'),
(101, 'Electrode', 50, 70, 'electric', 20, '101 Electrode.png'),
(103, 'Exeggutor', 95, 85, 'grass', 20, '103 Exeggutor.png'),
(104, 'Cubone', 50, 95, 'ground', 20, '104 Cubone.png'),
(105, 'Marowak', 80, 110, 'ground', 20, '105 Marowak.png'),
(106, 'Hitmonlee', 120, 53, 'fighting', 20, '106 Hitmonlee.png'),
(107, 'Hitmonchan', 105, 79, 'fighting', 20, '107 Hitmonchan.png'),
(108, 'Lickitung', 55, 75, 'normal', 20, '108 Lickitung.png'),
(109, 'Koffing', 65, 95, 'poison', 20, '109 Koffing.png'),
(110, 'Weezing', 90, 120, 'poison', 20, '110 Weezing.png'),
(111, 'Rhyhorn', 85, 95, 'ground', 20, '111 Rhyhorn.png'),
(112, 'Rhydon', 130, 120, 'ground', 20, '112 Rhydon.png'),
(113, 'Chansey', 5, 5, 'normal', 20, '113 Chansey.png'),
(114, 'Tangela', 55, 115, 'grass', 20, '114 Tangela.png'),
(115, 'Kangaskhan', 95, 80, 'normal', 20, '115 Kangaskhan.png'),
(116, 'Horsea', 40, 70, 'water', 20, '116 Horsea.png'),
(117, 'Seadra', 65, 95, 'water', 20, '117 Seadra.png'),
(118, 'Goldeen', 67, 60, 'water', 20, '118 Goldeen.png'),
(119, 'Seaking', 92, 65, 'water', 20, '119 Seaking.png'),
(121, 'Starmie', 75, 85, 'water', 20, '121 Starmie.png'),
(122, 'Mr. mime', 45, 65, 'psychic', 20, '122 Mr Mime.png'),
(123, 'Scyther', 110, 80, 'bug', 20, '123 Scyther.png'),
(124, 'Jynx', 50, 35, 'ice', 20, '124 Jynx.png'),
(125, 'Electabuzz', 83, 57, 'electric', 20, '125 Electabuzz.png'),
(126, 'Magmar', 95, 57, 'fire', 20, '126 Magmar.png'),
(127, 'Pinsir', 125, 100, 'bug', 20, '127 Pinsir.png'),
(128, 'Tauros', 100, 95, 'normal', 20, '128 Tauros.png'),
(129, 'Magikarp', 10, 55, 'water', 20, '129 Magikarp.png'),
(130, 'Gyarados', 125, 79, 'water', 20, '130 Gyarados.png'),
(131, 'Lapras', 85, 80, 'water', 20, '131 Lapras.png'),
(133, 'Eevee', 55, 50, 'normal', 20, '133 Eevee.png'),
(135, 'Jolteon', 65, 60, 'electric', 20, '135 Jolteon.png'),
(136, 'Flareon', 130, 60, 'fire', 20, '136 Flareon.png'),
(137, 'Porygon', 60, 70, 'normal', 20, '137 Porygon.png'),
(138, 'Omanyte', 40, 100, 'rock', 20, '138 Omanyte.png'),
(139, 'Omastar', 60, 125, 'rock', 20, '139 Omastar.png'),
(140, 'Kabuto', 80, 90, 'rock', 20, '140 Kabuto.png'),
(141, 'Kabutops', 115, 105, 'rock', 20, '141 Kabutops.png'),
(142, 'Aerodactyl', 105, 65, 'rock', 20, '142 Aerodactyl.png'),
(143, 'Snorlax', 110, 65, 'normal', 20, '143 Snorlax.png'),
(144, 'Articuno', 85, 100, 'ice', 20, '144 Articuno.png'),
(145, 'Zapdos', 90, 85, 'electric', 20, '145 Zapdos.png'),
(146, 'Moltres', 100, 90, 'fire', 20, '146 Moltres.png'),
(147, 'Dratini', 64, 45, 'dragon', 20, '147 Dratini.png'),
(148, 'Dragonair', 84, 65, 'dragon', 20, '148 Dragonair.png'),
(149, 'Dragonite', 134, 95, 'dragon', 20, '149 Dragonite.png'),
(150, 'Mewtwo', 110, 90, 'psychic', 20, '150 Mewtwo.png'),
(151, 'Mew', 100, 100, 'psychic', 20, '151 Mew.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokemons`
--
ALTER TABLE `pokemons`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
