<?php 

	class Rest extends CI_Controller {

		public function index() {

		}

		public function get_pokemons() {

			header("Content-Type: application/json");

			$offset = ( isset($_GET['offset']) ) ? $_GET['offset'] : 0;
			$limit = ( isset($_GET['limit']) ) ? $_GET['limit'] : 20;

			$this->load->model('Pokemons_model', 'pokemons');
			$pokemons = $this->pokemons->get('', '', $offset, $limit);

			echo json_encode($pokemons); exit;
		}

	}

?>