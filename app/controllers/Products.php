<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function index() {
		redirect('products/browse');
	}

	public function browse() {

		$this->load->view('wrappers/head');
		$this->load->view('modules/navbar');
		$this->load->view('view_products');
		$this->load->view('wrappers/foot');

	}
}

?>