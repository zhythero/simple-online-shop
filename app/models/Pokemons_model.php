<?php 

class Pokemons_model extends CI_Model {

	private $tables = array(
			'pokemons' => 'pokemons'
		);
	
	public function get($where = '', $like = '', $offset = 0, $limit = 20) {
		
		if (!empty($where)) {
			foreach ($where as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if (!empty($like)) {
			foreach ($like as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		$result = $this->db->get($this->tables['pokemons'], $limit, $offset);

		return $result->result_array();
	}

	public function count($where = '', $like = '') {
		return $this->db->count_all_results($this->tables['pokemons']);
	}
}

 ?>